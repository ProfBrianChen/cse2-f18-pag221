//////////////Paula Gonzalez
///////Homework 01
//// CSE 02 WelcomeClass
///
public class WelcomeClass{
  
  public static void main(String args[]){
    ///prints WelcomeClass to terminal window
    
    System.out.println("  -----------");
    System.out.println("  | Welcome |");
    System.out.println("  -----------");
    
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-P--A--G--2--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
  
    System.out.println("My name is Paula and I am on the Lehigh tennis team");
  } 
}