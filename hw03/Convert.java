///Paula Gonzalez
///hw03
//CSE 002
//September 18, 2018


import java.util.Scanner;

//beginning of code
public class Convert{
  //main method required for every Java program
  public static void main(String[] args){
    //prints Convert to terminal window
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter the affected area in acres: ");
    double affectedArea = myScanner.nextDouble(); //double affected area in acres
    System.out.print("Enter the rainfall in the affected area: ");
    double rainfall = myScanner.nextDouble(); //double rainfall in gallons
    
    double milesSquared = .0015625 * affectedArea; //converting area to miles squared
    double miles = .0000157828 * rainfall; //converting gallons to miles
  System.out.println((miles * milesSquared) + " cubic miles");
    
  }
}