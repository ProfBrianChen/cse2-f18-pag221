///Paula Gonzalez
///hw03
//CSE 002
//September 18, 2018

import java.util.Scanner;

//beginning of code
public class Pyramid{
  //main method required for every Java program
  public static void main(String[] args){
    //prints Pyramid to terminal window
    Scanner myScanner = new Scanner(System.in);
    System.out.print("The square side of the pyramid is (input length): ");
    double squareSide = myScanner.nextDouble(); //double the square side
    System.out.print("The height of the pyramid is (input height): ");
    double height = myScanner.nextDouble(); //double the height
    
    int volume = (int)(squareSide * squareSide * height)/3; //formula for volume of a pyramid
    System.out.println("The volume inside the pyramid is: " + volume);
  }
}