//////////////Paula Gonzalez
///////Homework 02
//// CSE 02 Arithmetic
///September 11, 2018

public class Arithmetic {
  
  public static void main(String args[]){
    ///prints Arithmetic to terminal window
    
    int numPants = 3; // numbers of pairs of pants
    double pantsPrice = 34.98; // cost per pair of pants
    int numShirts = 2; // number of sweatshirts
    double shirtPrice = 24.99; // cost per shirt
    int numBelts = 1; // number of belts
    double beltCost = 33.99; // cost per belt
    double paSalesTax = 0.06; // the tax rate
    
    double totalCostOfPants = numPants * pantsPrice; // total cost of pants
    double totalCostOfShirts = numShirts * shirtPrice; // total cost of shirts
    double totalCostOfBelts = numBelts * beltCost; // total cost of belts
    
    double salesTaxPants = totalCostOfPants * paSalesTax; // sales tax charged on pants 
    double salesTaxShirts = totalCostOfShirts * paSalesTax; // sales tax charged on shirts 
    double salesTaxBelts = totalCostOfBelts * paSalesTax; // sales tax charged on belts 
    
    double totalCostOfPurhcases = totalCostOfPants + totalCostOfShirts + totalCostOfBelts; // total cost of purchases before tax
    double totalSalesTax = salesTaxPants + salesTaxShirts + salesTaxBelts; // total sales tax
    double totalPaid = totalCostOfPurhcases + totalSalesTax; // total paid for transaction including sales tax
   
    System.out.println("The total cost of pants is "+totalCostOfPants + " dollars");
    System.out.println("The total cost of shirts is "+totalCostOfShirts + " dollars");
    System.out.println("The total cost of belts is "+totalCostOfBelts + " dollars");
    System.out.println("The sales tax for buying pants is "+ ((int)(salesTaxPants * 100)) / 100.0 + " dollars");
    System.out.println("The sales tax for buying shirts is "+ ((int)(salesTaxShirts * 100)) / 100.0 + " dollars");
    System.out.println("The sales tax for buying belts is "+((int)(salesTaxBelts * 100)) / 100.0 + " dollars");
    System.out.println("The total cost of the purchases before tax is "+totalCostOfPurhcases + " dollars");
    System.out.println("The total sales tax is "+ ((int)(totalSalesTax * 100)) / 100.0 + " dollars");
    System.out.println("The total cost of the purchases including sales tax is "+((int)(totalPaid * 100)) / 100.0 + " dollars");
  }
}
    
    