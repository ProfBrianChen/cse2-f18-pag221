
public class typeCastingDemo{
	public static void main(String[] args){
		// First, we examine implicit casting:
		//
		// Java views as decimal-valued numbers as doubles
    		//float a = 1.23456; // What happens when we let a = 1.23456?
    		//float a = (float) 1.234567; 
    		//double b = (double) a;
		//float a = 1.23456f; // to indicate a float, we can also use 'f' at the end
		//float b = 1.234567;
	 	 //float a = (float) b;
		//float a = (float) 12345678933; // This will cause an error.  Why?
		//float a = (float) 1.23456;
		//System.out.println("This is b: " + b);
		//System.out.println("This is a: " + a);
		
		// Here we show how an int will implicitly cast
		// to a double
		//int b = 32;
		//double c;
		
		//c = b * 1.4;
		//System.out.println("This is c: " + c);

		// Here we show how a float will implicitly cast to a double
		//double g;
		//double h = 3.000679;
		
		//g = a * h;
		//System.out.println("This is g: " + g);

		// Here we show how primitive types cast to Strings
		//float var1 = (float) 3.2; // another way to indicate float
		//double var2 = 3.3;
		//int var3 = 1;
		//boolean var4 = true;
		//char var5 = 'c';
		// Each of these primitive types are implicitly cast to Strings when
		// using the following print statement
		//System.out.println(var1 + " " + var2 + " " + var3 + " " + var4 + " " + var5);
		
		// Now, we examine explicit casting:
		//
		//double myDoubleValue = 3.141592;
		// Here I am casting a double to an int:
		//int myInt = (int) myDoubleValue;
		//System.out.println("This is myDoubleValue after casting it to an int: " + myInt);
		// Here I am casting a double to a float:
		//float myFlt = (float)    myDoubleValue;
		//System.out.println("This is myDoubleValue after casting it to a float: " + myFlt);\
		
		// Now, let's try to explicitly cast a string to a double 
		//String myString = "omg";
		//double myDouble3 = (double) myString;
		// This will cause a COMPILER ERROR (so we won't even bother trying to compile it)

		//double myDouble1 = 3.2;
		//double myDouble2 = 4.9;
   		//int myValorig = (int)(myDouble1+myDouble2);
		//int myVal = (int) myDouble1 + (int) myDouble2;
		// myDouble1 is explicitly cast to an int.
		// myDouble1 is added to myDouble2, a double.
		//The sum is implicitly cast to double.
		//The resulting double cannot be implicitly cast to int (for myVal).  That results in a compiler error.
		// How can we fix this error? (First, comment out int myVal = (int) myDouble1 + myDouble2;)
		//int testVal = (int)
		//int myVal = (int) (myDouble1 + myDouble2);
		//System.out.println("This is myVal: " + myVal);
		
	}
}