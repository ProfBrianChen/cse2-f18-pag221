/////Paula Gonzalez
////lab09
//CSE 002 
//November 16, 2018

import java.util.Scanner; //set up import statement for scanner
import java.util.Random; //set up import statement for random

//beginning of code
   public class lab09{ 
     //main method required for every Java program
     public static void main(String[] args) {  
     //prints lab09 to terminal window 
       Scanner myScanner = new Scanner(System.in);
       int [] array0 = {0,1,2,3,4,5,6,7}; //literal integer array 
       int [] array1 = copy(array0); //first copy of 
       int [] array2 = copy(array0); //second copy
       inverter(array0); //passing array0 to inverter
       print(array0); //printing out array0
       inverter2(array1); //passing array1 to inverter2
       print(array1); //printing array1
       int [] array3 = inverter2(array2); //passing array1 to inverter2 and assigning the output to array3
       print(array3); //printing array3
     }


public static int [] copy(int [] array){      
  int [] b = new int [array.length]; //declare and creates the integer array   
  for(int i=0;i<array.length;i++){ //copying one array to another
    b[i]=array[i]; //setting b equal to array
  }
  return b;
}


public static void inverter(int [] array){ //method that reverses the order of an array
  for(int i = 0; i < array.length / 2; i++){ 
    int temp = array[i]; //assigning a value to temp
    array[i] = array[array.length - i - 1]; //assigning the value of array[i]
    array[array.length - i - 1] = temp; //setting the value of the array equal to temp
}
}
   

public static int [] inverter2(int [] array){ //methiod that makes copy and inverts copy
  int [] array1 = copy(array); //makes copy
  inverter(array1); //inverts
  return array1;
}
  
  
   public static void print(int [] array){ //method that prints
     for(int i=0; i<array.length; i++){ 
       System.out.print(array[i] + " ");
     }
     System.out.println();
   }
  
  
   }
  
  
  
  