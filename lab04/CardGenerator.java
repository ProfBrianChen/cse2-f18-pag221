////Paula Gonzalez
///Lab04
//CSE02 CardGenerator
//September 21, 2018

import java.util.Random;

///beginning of code
public class CardGenerator{
  //main method required for every Java program
  public static void main(String[] args){
    //prints CardGenerator to terminal window
    int upperBound = 52; //assigned the upper bound
    int baseNum = 1; //assigned baseNum
    int card = (int)(Math.random()*(upperBound))+baseNum; //assigned card
    System.out.println("The number is: "+ card);
    String suit = "Hi";
    
    if (card <= 13){
      suit = "diamonds";
    }
    else if (card <= 26){
      suit = "clubs";
    }
    else if (card <= 39){
      suit = "hearts";
    }
    else if (card <= 52){
      suit = "spades";
    }
    //if and if-else statements for each suit
 
    String cardString;
    switch (card%13){
      case 1: cardString = "Ace";
        break;
      case 2: cardString = "2";
        break;
      case 3: cardString = "3";
        break;
      case 4: cardString = "4";
        break;  
      case 5: cardString = "5";
        break;
      case 6: cardString = "6";
        break;
      case 7: cardString = "7";
        break;
      case 8: cardString = "8";
        break;
      case 9: cardString = "9";
        break;
      case 10: cardString = "10";
        break;
      case 11: cardString = "Jack";
        break;
      case 12: cardString = "Queen";
        break;
      case 0: cardString = "King";
        break;
      default: cardString = "Invalid card";
        break;
    } 
    //switch statements for every card identity
    
    System.out.println("You picked the "+cardString+" of "+suit);
   
  }
}