/// Paula Gonzalez
/// September 7, 2017
/// CSE 002-110

public class Cyclometer {
  // main method required for every Java program
  public static void main (String [] args){
    // input data
    
    int secsTrip1 = 480; // time elapsed in seconds in the first trip 
    int secsTrip2 = 3220; // time elapsed in seconds in the second trip
    int countsTrip1 = 1561; // number of counts in trip 1
    int countsTrip2 = 9037; // number of counts in trip 2
    
    // declaring more values
    
    double wheelDiameter = 27.0, // double the diameter of the wheel
    PI = 3.14159, // declaring the value of pi
    feetPerMile = 5280, // declaring how many feet in a mile
    inchesPerFoot = 12, // declaring how many inches in a foot
    secondsPerMinute = 60; // declaring how many seconds in a minute
    double distanceTrip1, distanceTrip2, totalDistance; // double the two distances and the totalDistanc
    
    // print out numbers stored in the variables
    System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+ " counts.");
    System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had " +countsTrip2+ " counts.");
    // Trip 1 took 8.0 minutes and had 1561 counts.
    // Trip 2 took 53.666666666666664 minutes and had 9037 counts.
    // In the above printed statement I calculated the amount of minutes and counts that each trip took
    
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; // gives distance in inches (for each count, a rotation of the wheel travels the diameter in inches times PI) 
    distanceTrip1 /= inchesPerFoot * feetPerMile; // gives distance in miles
    distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile;
    totalDistance = distanceTrip1 + distanceTrip2;
    
    // print out the output data
    System.out.println("Trip 1 was "+distanceTrip1+" miles" );
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
    
  }
}