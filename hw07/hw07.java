/////Paula Gonzalez
////hw07
//CSE 002 
//November 5, 2018

import java.util.Scanner; //set up import statement for scanner
import java.util.Random; //set up import statement for random

///beginning of code
  public class hw07{
    //main method required for every Java program
    public static void main(String[] args){
      //prints hw07 to terminal window 
      Scanner myScanner = new Scanner(System.in); 
      String inputString = ""; //initializing inputString 
      char menuOption = ' '; //initializing menuOption
      String toFind = ""; //initializing toFind
      
      System.out.println("Enter a sample text: ");
      inputString = myScanner.nextLine(); //waits for sample text to be entered before continuing to next print statement
      System.out.println("You entered: " + inputString);
      
      while(menuOption != 'q'){ //beginning of while loop
        menuOption = printMenu(myScanner);
        if(menuOption == 'c'){
          System.out.println("Number of non-white characters: " + getNumOfNonWSCharacters(inputString)); //print statement if they choose menuOption c
        }
        else if(menuOption == 'w'){
          System.out.println("Number of words: " + getNumOfWords(inputString)); //print statement if they choose menuOption w
        }
        
        else if(menuOption == 'f'){
          System.out.println("Enter a word or phrase that needs to be found: ");
          toFind = myScanner.nextLine(); //waits for word or phrase that needs to be found to be entered before continuing to next print statement
          System.out.println(toFind + " instances: " + findText(toFind, inputString)); //print statement if they choose menuOption f
        }
        
        else if(menuOption == 'r'){
          System.out.println("Edited text: " + replaceExclamation(inputString)); //print statement if they choose menuOption r
        }
        
        else if(menuOption == 's'){
          System.out.println("Edited text: " + shortenSpace(inputString)); //print statement if they chosoe menuOption s
        }
        return;
      } //ends while loop for different menu options
    } //ends main method
      
    
      public static char printMenu(Scanner myScanner){
        char menu = ' '; //initializing the menu 
        System.out.println("Menu");
        System.out.println("c - Number of non-whitespace characters");
        System.out.println("w- Number of words");         
        System.out.println("f- Find text");         
        System.out.println("r- Replace all !'s");         
        System.out.println("s - Shorten spaces");         
        System.out.println("q- Quit");         
        System.out.println();
        while(menu != 'c' && menu != 'w' && menu != 'f' && menu != 'r' && menu != 's' && menu != 'q'){ //while you don't type in one of these options 
          System.out.println("Choose one of the options above");
          menu = myScanner.nextLine().charAt(0); //character input in a new line
        } //ends while statement
        return menu; //returns the value of the menu
      } //ends printMenu
      
      
      public static int getNumOfNonWSCharacters(final String str){
           int counter = 0; //initializing counter
           
           for(int i=0; i<str.length(); i++){ //the length of the string
             if(str.charAt(i) != ' '){ //char at position i in the string
               ++counter; //increase counter
             }
           }
           return counter; //returns counter
      } //ends getNumOfNonWSCharacters
      
      
      public static int getNumOfWords(final String str){
        int counter=0; //initializing counter
        
        for(int i=1; i<str.length(); i++){ //the length of the string
          if((Character.isWhitespace(str.charAt(i))==true) && (Character.isWhitespace(str.charAt(i-1)))==false){ //determines if the character is whitespace according to Java
               ++counter; //increase counter
             }
             }
        for(int i=1; i<str.length(); i++){ //the length of the string
          if(Character.isWhitespace(str.charAt(i-1))==false){ //determines if the character is whitespace according to Java
          ++counter; //increase counter
           }
        }
          return counter; //returns counter
      } //ends getNumOfWords
      
      
      public static int findText(final String toFind, String str){
      int counter = 0; //initializing counter
      int location = 0; //initializing location
      
      do{
        location = str.indexOf(toFind); //returns the position of the first occurance of a specified value, toFind, in a string
        if(location == -1){ //-1 if the value to search for never occurs
          return counter; //returns counter
        }
        counter++; //increase counter
          str = str.substring(location +1, str.length()); //returns part of string between start, end
      } while(location >= 0); //while location exists
      
      return counter; //returns counter
      } //ends findText
      
      
      public static String replaceExclamation(String str){
        return str.replace('!','.'); //replaces ! with .
      } //ends replaceExclamation
      
      
      public static String shortenSpace(String str){
        while(str.indexOf(" ") != -1){ //returns the position of the first occurance of a specified value in a string
          str = str.replace(" "," "); //
        }
        return str;
      } //ends shortenSpace
      
      }
        