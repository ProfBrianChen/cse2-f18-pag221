/////Paula Gonzalez
////lab07
//CSE 002 
//November 9, 2018

import java.util.Scanner; //set up import statement for scanner
import java.util.Random; //set up import statement for random

///beginning of code
  public class lab08{
    //main method required for every Java program
    public static void main(String[] args){
      //prints lab08 to terminal window 
      
      Random randomGenerator = new Random(); //creates random object
      int [] firstArray = new int [100]; //initializing first array of size 100
      int [] secondArray = new int[100]; //initializing second array of size 100
      
      for(int i=0; i<firstArray.length; i++){ //for loop for randomly assigning integers to the first array
      firstArray[i] = (int)(Math.random() * 100); //fill first array with random integers range 0-99
      System.out.println(firstArray[i]); //print out elements in the first array
      }      
      
      for(int i : firstArray){ //for each element i in first array
        secondArray[i]++; //increase second array
      }
      
      for(int i=0; i<secondArray.length; i++){  //for loop for second array
      System.out.println("" + i + " occurs " + secondArray[i] + " times");
      }
      
    }
  }
  