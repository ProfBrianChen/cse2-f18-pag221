/////Paula Gonzalez
////lab07
//CSE 002 
//October 26, 2018

import java.util.Scanner; //set up import statement for scanner
import java.util.Random; //set up import statement for random

///beginning of code
  public class Methods{
    //main method required for every Java program
    public static void main(String[] args){
      //prints Methods to terminal window 
      Scanner myScanner = new Scanner(System.in);
      Random randomGenerator = new Random (); //create random object
      String word1 = " "; //initializing adjective  
      String word2 = " "; //initializing adjective
      String word3 = " "; //initializing nounForSubject      
      String word4 = " "; //initializing verb
      String word5 = " "; //initializing adjective
      String word6 = " "; //initializing nounForObject
      String subject = subjectSentence(); //prints subject
      String conclusion = " ";
      System.out.println("Press 1 if you would you like another sentence or 2 if you want to quit "); //what does user want?
      int sentence = myScanner.nextInt();
      do {
        System.out.println("The " + "<" + (word1 = adjective()) + "> " + "<" + (word2 = adjective())
                           + "> " + "<" + (word3 = nounForSubject()) + "> " + "<" + (word4 = verb()) 
                           + "> " + "the " + "<" + (word5 = adjective()) + "> " + "<" + (word6 = nounForObject()) + ">. ");
        System.out.println("Press 1 if you would you like another sentence or 2 if you want to quit ");
        sentence = myScanner.nextInt();
      } while (sentence == 1); 
      System.out.println(finalSentence(subject));
    
    }
              
      public static String adjective(){ //method for adjective         
        Random randomGenerator = new Random(); //creates random object
        int word1 = randomGenerator.nextInt(10); //generates random integers less than 10         
        //System.out.print(word1);
        String word1String = ""; //initialized word1String
        switch(word1){
          case 1: 
            word1String = "brave";
            break;
          case 2:
            word1String = "calm";
            break;
          case 3:
            word1String = "little";
            break;
          case 4:
            word1String = "large";
            break;
          case 5:
            word1String = "fancy";
            break;
          case 6:
            word1String = "elegant";
            break; 
          case 7:
            word1String = "plain";
            break; 
          case 8:
            word1String = "beautiful";
            break; 
          case 9:
            word1String = "young";
            break; 
          case 0:
            word1String = "large";
            break;
        } //switch statement for adjective
        return word1String;
      }
      
      public static String nounForSubject(){ //method for adjective         
        Random randomGenerator = new Random(); //creates random object
        int word2 = randomGenerator.nextInt(10); //generates random integers less than 10         
        //System.out.print(word2);
        String word2String = ""; //initialized word2String
        switch(word2){
          case 1: 
            word2String = "dog";
            break;
          case 2:
            word2String = "cat";
            break;
          case 3:
            word2String = "teacher";
            break;
          case 4:
            word2String = "daughter";
            break;
          case 5:
            word2String = "son";
            break;
          case 6:
            word2String = "elephant";
            break; 
          case 7:
            word2String = "bird";
            break; 
          case 8:
            word2String = "parrot";
            break; 
          case 9:
            word2String = "horse";
            break; 
          case 0:
            word2String = "grandpa";
            break;
        } //switch statement for non primary nouns appropriate for the subject of the sentence
        return word2String;
      } 
      
      public static String verb(){ //method for adjective
        Random randomGenerator = new Random(); //creates random object
        int word3 = randomGenerator.nextInt(10); //generates random integers less than 10         
        //System.out.print(word3);
        String word3String = ""; //initialized word3String
        switch(word3){
          case 1: 
            word3String = "sat";
            break;
          case 2:
            word3String = "ran";
            break;
          case 3:
            word3String = "walked";
            break;
          case 4:
            word3String = "grabed";
            break;
          case 5:
            word3String = "smiled";
            break;
          case 6:
            word3String = "rode";
            break; 
          case 7:
            word3String = "sang";
            break; 
          case 8:
            word3String = "shouted";
            break; 
          case 9:
            word3String = "slapped";
            break; 
          case 0:
            word3String = "hopped";
            break;
        } //switch statement for verb
        return word3String;
      } 
      
      public static String nounForObject(){ //method for adjective
        Random randomGenerator = new Random(); //creates random object
        int word4 = randomGenerator.nextInt(10); //generates random integers less than 10
        //System.out.print(word4);
        String word4String = ""; //initialized word4String
        switch(word4){
          case 1: 
            word4String = "bike";
            break;
          case 2:
            word4String = "plants";
            break;
          case 3:
            word4String = "president";
            break;
          case 4:
            word4String = "sailors";
            break;
          case 5:
            word4String = "student";
            break;
          case 6:
            word4String = "adult";
            break; 
          case 7:
            word4String = "grandma";
            break; 
          case 8:
            word4String = "grandpa";
            break; 
          case 9:
            word4String = "child";
            break; 
          case 0:
            word4String = "adolescent";
            break;
        } //switch statement for non primary nouns appropriate for the object of the sentence
        return word4String;
      }
      
      public static String subjectSentence(){ //creates sentence 1 and passes subj
      String subject = nounForSubject(); 
      System.out.println("The " + adjective() + " " + adjective() + " " + subject + " " + verb() + " the "+ adjective() + " " + nounForObject() + ".");
      return subject;
    }
      
      public static String actionSentence(String subject){ //randomly selects it
        Random randomGenerator = new Random(); //creates random object
        int subjectInt = randomGenerator.nextInt(2); //generates random integers less than 2
        
        String subjectString = subject;    
        String action = subjectString + " " + "used" + " " + nounForObject() + " " + "to " + verb() + " the " + nounForObject() + " at the " + adjective() + " " + nounForSubject() + ".";
        return action;
      }
      
      public static String conclusionSentence(String nounForSubject){ //creates conclusion sentence
        String conclusion = " That " + nounForSubject() + " " + verb() + " the " + nounForObject() + "!";
        return conclusion;
      }
      
      public static String finalSentence(String subject){ //creates final sentence
        Random randomGenerator = new Random(); //creates random object
        int actionTotal = randomGenerator.nextInt(5); //generates random integers less than 5
        int action=0; 
        
        while(action < actionTotal){
          System.out.println(actionSentence(subject));
          action++;
        }
        
        String finalSentence = actionSentence(subject) + conclusionSentence(subject); //creates final sentence by combining action and conclusion sentence
        return finalSentence;
      }
      
      }