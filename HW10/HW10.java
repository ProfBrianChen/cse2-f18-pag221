/////Paula Gonzalez
////HW10
//CSE 002 
//December 4, 2018

import java.util.Scanner; //set up import statement for scanner
import java.util.Random; //set up import statement for random

//beginning of code
public class HW10{ 
  //main method required for every Java program
  public static void main(String[] args) {  
    //prints HW10 to terminal window 
    
    String [][] game = {{"1","2","3"},{"4","5","6"},{"7","8","9"}}; //creating multidimensional array of size 3 by 3
    Scanner myScanner = new Scanner(System.in);
    
    while(check(game)==false){ //while loop for the board, O, and X
      for(int i=0; i<game.length; i++){ //rows
        for(int j=0; j<game.length; j++){ //columns
          System.out.print(game[i][j] + " "); //prints out board
        }
        System.out.println(); //print statement
      }
      
      System.out.println("Enter the placement of the O token: "); //print statement
      String position=myScanner.next(); //initialize position
      
      //if statement for checking whether input is out of bounds or an integer
      if(position.equals("1") || position.equals("2") || position.equals("3") || position.equals("4") || position.equals("5") || position.equals("6") || position.equals("7") || position.equals("8") || position.equals("9")){
      }
      else{
        System.out.println("Invalid Input. It's either out of bounds or not an integer"); //print statement
        String junkword = myScanner.next();
      }
      
      //loop for O
      for(int i=0; i<game.length; i++){ //rows
        for(int j=0; j<game.length; j++){ //columns
          if(game[i][j].equals(position)){
            game[i][j]="O"; //print O
          }
        }
      }
      
      if (check(game) == true){ //if statement that doesn't let you keep going after player wins
        break;
      }
      
      //loop for X
      System.out.println("Enter the placement of the X token: "); //print statement
      position = myScanner.next(); //enter a position
      for(int i=0; i<game.length; i++){ //rows
        for(int j=0; j<game.length; j++){ //columns
          if(game[i][j].equals(position)){
            game[i][j]="X"; //print X
          }
        }
      }
      check(game); //calls check
    } 
  }
  
  
  public static void print(String [][]game){ //print method
    for(int i=0; i<game.length; i++){ //rows
      for(int j=0; j<game.length; j++){ //columns
        System.out.print(game[i][j] + " "); //print statement
      }
      System.out.println(); //print statement
    }
  }
  
  public static boolean check(String [][]game){ //checks all possible conditions for whether player wins or if there's a draw
    if(game[0][0]==game[1][0] && game[1][0]==game[2][0]){ //condition if player wins
      print(game); //calls print method
      System.out.println("You've won!"); //print statement
      return true; //return statement
    }
    
    if(game[0][1]==game[1][1] && game[1][1]==game[2][1]){ //condition if player wins
      print(game); //calls print method
      System.out.println("You've won!"); //print statement
      return true; //return statement
    }
    
    if(game[0][2]==game[1][2] && game[1][2]==game[1][2]){ //condition if player wins
      print(game); //calls print method
      System.out.println("You've won!"); //print statement
      return true; //return statement
    }
    if(game[0][0]==game[0][1] && game[0][1]==game[0][2]){ //condition if player wins
      print(game); //calls print method
      System.out.println("You've won!"); //print statement
      return true; //return statement
    }
    
    if(game[1][0]==game[1][1] && game[1][1]==game[1][2]){ //condition if player wins
      print(game); //calls print method
      System.out.println("You've won!"); //print statement
      return true; //return statement
    }
    
    if(game[2][0]==game[2][1] && game[2][1]==game[2][2]){ //condition if player wins
      print(game); //calls print method
      System.out.println("You've won!"); //print statement
      return true; //return statement
    }
    
    if(game[0][0]==game[1][1] && game[1][1]==game[2][2]){ //condition if player wins
      print(game); //calls print method
      System.out.println("You've won!"); //print statement
      return true; //return statement
    }
    
    if(game[0][2]==game[1][1] && game[1][1]==game[2][0]){ //condition if player wins
      print(game); //calls print method
      System.out.println("You've won!"); //print statement
      return true; //return statement
    }
    
    else{ //condition if neither player wins
      print(game); //calls print method
      System.out.println("There is a draw, no winner."); //print statement
      return false; //return statement
    }
    
  }
}


