/////Paula Gonzalez
////Lab03
//CSE 02 Check
//September 14, 2018

import java.util.Scanner;

///beginning of code
public class Check{
  //main method required for every Java program
  public static void main(String[] args){
    //prints Check to terminal window
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
      double checkCost = myScanner.nextDouble(); //double check cost
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble(); //double tip percent
    tipPercent /= 100; //we want to convert the percentage into a decimal value
      System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt(); //inputing the number of people
    
    //print output
    double totalCost; //double total cost
    double costPerPerson; //double cost per person
    int dollars; //whole dollar amount of cost
    totalCost = (int)checkCost * (1 + tipPercent); //calculating the total cost
    costPerPerson = totalCost / numPeople; //get the whole amount, dropping decimal fraction 
    dollars = (int)costPerPerson; //calculating the dollars
    int dimes = (int)(costPerPerson * 10) % 10; //calculating the dimes
    int pennies = (int)(costPerPerson * 100) % 10; //calculating the pennies
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
    
    
  } //end of main method
} //end of class