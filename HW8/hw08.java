/////Paula Gonzalez
////hw08
//CSE 002 
//November 15, 2018

import java.util.Scanner; //set up import statement for scanner
import java.util.Random; //set up import statement for random

public class hw08{ 
  
public static void main(String[] args) { 
  
  //code given by Professor Carr
Scanner myScanner = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
  String[] suitNames={"C","H","S","D"}; //initializing suit name   
  String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; //initializing rank name
  String[] cards = new String[52]; //initializing array of size 52
  String[] hand = new String[5]; //initializing array of size 5
  int numCards = 5; //initializing number of cards
  int again = 1; //initializing again
  int index = 51; //initializing index
  for (int i=0; i<52; i++){ //for loop for cards
    cards[i]=rankNames[i%13]+suitNames[i/13]; 
    System.out.print(cards[i]+" ");
  } 
  System.out.println();
  printArray(cards); //relating to my first method
  shuffle(cards); //relating to my second method
  printArray(cards); 
  while(again == 1){ 
    hand = getHand(cards,index,numCards); //relating to my third method
    printArray(hand);
    index = index-numCards;
    System.out.println("Enter a 1 if you want another hand drawn"); 
    again = myScanner.nextInt(); 
}
}


public static void printArray(String [] cardsArray){
  for (int i=0; i<cardsArray.length; i++){ 
  System.out.print(cardsArray[i]+" "); //prints all the cards
}
}
  
  public static void shuffle(String [] cardsArray){
  System.out.println("\nShuffled"); //prints out shuffled
  for(int i=0; i<cardsArray.length-1; i++){ //for loop to shuffle and swap
    int j = (int)Math.floor(Math.random()*cardsArray.length); //randomly generates index j
    String temp= cardsArray[i]; //stores the value
    cardsArray[i]=cardsArray[j]; //copy the card
    cardsArray[j]=temp; //replace the card
}
}
  
  
public static String [] getHand(String [] cardsArray,int index,int numCards){
  System.out.println("\nHand"); //prints out hand
  String [] hand = new String [numCards]; //array that holds the number of cards in numCards
  for(int i=0; i<numCards; i++){ //for loop for the hands
    if(index<0){
      hand[i]= "noCardsAvailable";
    }
    else{
      hand[i] = cardsArray[index--];
    }
  }
      return hand; //returns hand value to main method
  } 

}