/////Paula Gonzalez
////hw09
//CSE 002 
//November 27, 2018

import java.util.Scanner; //set up import statement for scanner
import java.util.Random; //set up import statement for random

//beginning of code
   public class RemoveElements{ 
     //main method required for every Java program
     public static void main(String[] args) {  
     //prints RemoveElements to terminal window 
       Scanner scan=new Scanner(System.in);
       //code given by Professor Carr
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
 String answer="";
 do{
   System.out.print("Random input 10 ints [0-9]");
   num = randomInput();
   String out = "The original array is:";
   out += listArray(num);
   System.out.println(out);
 
   System.out.print("Enter the index ");
   index = scan.nextInt();
   newArray1 = delete(num,index);
   String out1="The output array is ";
   out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
   System.out.println(out1);
 
      System.out.print("Enter the target value ");
   target = scan.nextInt();
   newArray2 = remove(num,target);
   String out2="The output array is ";
   out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
   System.out.println(out2);
    
   System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
   answer=scan.next();
 }while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
    String out="{";
    for(int j=0;j<num.length;j++){
      if(j>0){
        out+=", ";
      }
      out+=num[j];
    }
    out+="} ";
    return out;
  }
  
  
  public static int [] randomInput(){ //random input method
    int array [] = new int [10]; //create array of length 10
    for(int i = 0; i < array.length; i++){
      array[i] = (int)(Math.random()*(array.length-1)); //fills array with random int
    }
    return array; //return array
  }
  
  
  public static int [] delete(int [] list, int pos){ //delete method
    Scanner scan=new Scanner(System.in);
    boolean b = false; //assign value to boolean
    if(pos < 0 || pos > 9){ //if pos less than 0 or greater than 9
      System.out.println("Out of bounds"); //print statement
      String junkword = scan.next();
      pos = scan.nextInt();
    }
    int [] newList = new int [list.length-1]; //assign value to newList
    for (int i = 0; i < newList.length; i++){          
      if (pos == i){ //pos equal to i
        b = true;
      }
      if(b){ //if true
        newList[i] = list[i+1];
      }
      else{ //if not true
        newList[i] = list[i];
      }
    }
    return newList; //return newList
       }
  
  
  public static int [] remove(int [] list, int target){ //remove method
    int counter = 0; //assign value to counter
    for (int i = 0; i < list.length; i++){
      if(target == list[i]){ //target equal to list[i]
        counter++; //increase counter
      }
    }
    int [] newList = new int [list.length - counter]; //creating newList
    int j = 0; //assigns value to j
    for(int i = 0; i < newList.length; i++){ 
      if(target == list[j]){ //target equal to list[j]
        j++; //increase j
      }
      newList[i] = list[j]; //assigning newList[i] equal to list[j]
      j++; //increase j
    }
    return newList; //return newList
  }
   }
         
         
         
         