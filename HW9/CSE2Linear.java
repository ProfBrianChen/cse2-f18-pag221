/////Paula Gonzalez
////hw09
//CSE 002 
//November 27, 2018

import java.util.Scanner; //set up import statement for scanner
import java.util.Random; //set up import statement for random

//beginning of code
   public class CSE2Linear{ 
     //main method required for every Java program
     public static void main(String[] args) {  
     //prints CSE2Linear to terminal window 
       int [] grades = new int[15]; //array size 15
       Scanner myScanner = new Scanner(System.in);
       System.out.println("Enter 15 ascending ints for final grades in CSE2: "); //print statement
       
       for(int i = 0; i < grades.length; i++){
         if(!myScanner.hasNextInt()){ //if not an int
           System.out.println("Please enter an integer"); //print statement
           String junkword = myScanner.next();
         }
         
       
      grades[i]= myScanner.nextInt(); //assign value to array
         if(grades [i] < 0 || grades [i] >100){ //if less than 0 or greater than 100
           System.out.println("Integer needs to be between 0 and 100"); //print statement
           String junkword = myScanner.next();
         }
       
         if(i != 0 && grades[i] < grades[i-1]){ //if not 0 and the int isn't greater than or equal to the last int
             System.out.println("Integer is not greater than or equal to the last int"); //print statement
             String junkword = myScanner.next();
         }
       }
       
       for(int i=0 ; i < grades.length; i++){ //for loop for printing
         System.out.print(grades[i]+ " "); //print statement
         }
         System.out.println(); //print statement
     
       System.out.println("Enter a grade to search for: "); //print statement
       int key = myScanner.nextInt(); //assign value to key
       binarySearch(grades, key); //calling method binary search
       scramble(grades); //calling method scramble
     }
     
       
     public static void binarySearch(int[] list, int key) { //binary search method from Professor Carr
       int counter = 0; //assign value to counter
       int low = list[0]; //assign value to low
       int high = list.length-1; //assign value to high
       while(high >= low) { //while loop
         ++counter; //increase counter
         int mid = (low + high)/2; //assign value to mid
         if (key < list[mid]) {
           high = mid - 1;
        }
        else if (key == list[mid]) {
           System.out.println(key + " was found in the list with " + counter + " iterations"); //print statement
           return; //return statement
         }
         else {
           low = mid + 1;
         }
         
       }
       System.out.println(key + " was not found in the list with " + counter + " iterations"); //print statement
     }     
     
     
     public static void linearSearch(int[] list, int key) { //linear search method
       int counter = 0; //assign value to counter
       for (int i = 0; i < list.length; i++){
         counter++; //increase counter
         if (key == list[i]){ 
           System.out.println(key + " was found in the list with " + counter + " iterations"); //print statement
           return; //return statement
           }
       }
       System.out.println(key + " was not found in the list with " + counter + " iterations"); //print statement
       }
     
     
     public static void scramble(int [] grades){
       Scanner myScanner = new Scanner(System.in);
       System.out.println("\nScrambled"); //prints out scrambled
       for(int i=0; i<grades.length-1; i++){ //for loop to scrambled
         int j = (int)Math.floor(Math.random()*grades.length); //randomly generates index j
         int temp = grades[i]; //stores the value
         grades[i] = grades[j]; //copy the card
         grades[j] = temp; //replace the card
}
       for(int i=0 ; i < grades.length; i++){
         System.out.print(grades[i]+ " "); //print statement
         }
         System.out.println(); //print statement
         System.out.println("Enter a grade to search for: "); //print statement
         linearSearch(grades, myScanner.nextInt()); //calling linear search
}
     }
     