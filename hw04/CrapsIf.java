//// Paula Gonzalez
/// hw04
/// CSE 02 CrapsIf
// September 25, 2018

import java.util.Scanner;

//beginning of code
public class CrapsIf {
   //main method required for every Java program
    public static void main(String[] args) {
      //print CrapsIf to terminal window
      Scanner myScanner = new Scanner(System.in);
      System.out.println("press 1 if you'd like to randomly cast dice or press 2 if you would like to state the two dice");
      int question = myScanner.nextInt(); //assigned question
      int die1 = 0; //assigned die1
      int die2 = 0; //assigned die2
      int roll = 0; //assigned roll
        
       if (question == 1){
         die1 = (int)(Math.random()*6) + 1;
         die2 = (int)(Math.random()*6) + 1;
         roll = die1 + die2;
      }
      else if (question == 2){
        die1 = myScanner.nextInt();
        die2 = myScanner.nextInt();
      } //if and else if statement for randomly casting or for stating the two dice
      
      System.out.println("The first die is " + die1);
      System.out.println("The second die is " + die2);

      if (roll == 1){
        System.out.println("Snake Eyes");
      }
      else if (die1 == 2 && die2 == 2){
        System.out.println("Hard Four");
      }
      else if (die1 == 2 && die2 == 1 || die1 == 1 && die2 ==2){
        System.out.println("Ace Deuce");
      }
      else if (die1 == 1 && die2 == 3 || die1 == 3 && die2 == 1){
        System.out.println("Easy Four");
      }
      else if (die1 == 3 && die2 == 2 || die1 == 2 && die2 == 3){
        System.out.println("Fever Five");
      }
      else if (die1 == 1 && die2 == 4 || die1 == 4 && die2 == 1){
        System.out.println("Fever Five");
      }
      else if (die1 == 3 && die2 == 3){
        System.out.println("Hard Six");
      }
      else if (die1 == 2 && die2 == 3 || die1 == 3 && die2 == 2){
        System.out.println("Fever Five");
      }
      else if (die1 == 2 && die2 == 4 || die1 == 4 && die2 == 2){
        System.out.println("Easy Six");
      }
      else if (die1 == 5 && die2 == 2 || die1 == 2 && die2 ==5){
        System.out.println("Seven Out");
      }
      else if (die1 == 1 && die2 == 5 || die1 == 5 && die2 == 1){
        System.out.println("Easy Six");
      }
      else if (die1 == 3 && die2 == 4 || die1 == 4 && die2 == 3){
        System.out.println("Seven Out");   
      }
      else if (die1 == 3 && die2 == 5 || die1 == 5 && die2 == 3){
        System.out.println("Easy Eight");
      }
      else if (die1 == 4 && die2 == 5 || die1 == 5 && die2 == 4){
        System.out.println("Nine");
      }
      else if (die1 == 5 && die2 == 5 ){
        System.out.println("Hard Ten");
      }
      else if (die1 == 1 && die2 == 6 || die1 == 6 && die2 == 1){
        System.out.println("Seven Out");
      }
      else if (die1 == 2 && die2 == 6 || die1 == 6 && die2 == 2){
        System.out.println("Easy Eight");
      }
      else if (die1 == 3 && die2 == 6 || die1 == 6 && die2 == 3){
        System.out.println("Nine");
      }
      else if (die1 == 4 && die2 == 6 || die1 == 6 && die2 == 4){
        System.out.println("Easy Ten");
      }
      else if (die1 == 5 && die2 == 6 || die1 == 6 && die2 == 5){
        System.out.println("Yo-leven");
      }
      else if (die1 == 6 && die2 == 6){
        System.out.println("Boxcars");
      }
      else if (die1 == 4 && die2 == 4){
        System.out.println("Hard Eight");
      }
      else {
        System.out.println("Invalid input! Try rolling again");
      }
    } //if and if-else statements for all the different combinations of dice and their respective slang terminology
}