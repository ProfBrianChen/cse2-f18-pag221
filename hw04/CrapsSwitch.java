//// Paula Gonzalez
/// hw04
/// CSE 02 CrapsSwitch
// September 25, 2018

import java.util.Scanner;

//beginning of code
public class CrapsSwitch {
   //main method required for every Java program
    public static void main(String[] args) {
      //print CrapsSwitch to terminal window
      Scanner myScanner = new Scanner(System.in);
      System.out.println("press 1 if you'd like to randomly cast dice or press 2 if you would like to state the two dice");
      int question = myScanner.nextInt(); //assigned question
      int die1 = 0; //assigned die
      int die2 = 0; //assigned die2
      int roll = 0; //assigned roll
     
      String questionString;
      switch(question){
        case 1:
          die1 = (int)(Math.random()*6) + 1;
          die2 = (int)(Math.random()*6) + 1;
          roll = die1 + die2;
          break;
        case 2:
          die1 = myScanner.nextInt();
          die2 = myScanner.nextInt();
          break;
      } //switch statements for randomly wanting to cast dice or for stating the two dice 
      
      System.out.println("The first die is " + die1);
      System.out.println("The second die is " + die2);
      
      String die1String = ""; //initialized die1String
      String die2String = " "; //initialized die2String
      switch(die1){
        case 1: 
          switch(die2){
            case 1: die2String = "Snake eyes";
              break;
            case 2: die2String = "Ace Deuce";
              break;
            case 3: die2String = "Easy Four";
              break;
            case 4: die2String = "Fever Five";
              break;
            case 5: die2String = "Easy Six";
              break;
            case 6: die2String = "Seven Out";
              break;
            default: die2String = "Invalid";
              break;
          } //switch statement for die1 = 1 with all the different options for die2
        case 2:
           switch(die2){
             case 1: die2String = "Ace Deuce";
               break;
             case 2: die2String = "Hard Four";
               break;
             case 3: die2String = "Fever Five";
               break;
             case 4: die2String = "Easy Six";
               break;
             case 5: die2String = "Seven Out";
               break;
             case 6: die2String = "Easy Eight";
               break;
             default: die2String = "Invalid";
               break;
           } //switch statement for die1 = 2 with all the different options for die2
        case 3:
           switch(die2){
             case 1: die2String = "Easy Four";
               break;
             case 2: die2String = "Fever Five";
               break;
             case 3: die2String = "Hard Six";
               break;
             case 4: die2String = "Seven Out";
               break;
             case 5: die2String = "Easy Eight";
               break;
             case 6: die2String = "Nine";
               break;
             default: die2String = "Invalid";
           } //switch statement for die1 = 3  with all the different options for die2
        case 4:
          switch(die2){
            case 1: die2String = "Fever Five";
              break;
            case 2: die2String = "Easy Six";
              break;
            case 3: die2String = "Seven Out";
              break;
            case 4: die2String = "Hard Eight";
              break;
            case 5: die2String = "Nine";
              break;
            case 6: die2String = "Easy Ten";
              break;
            default: die2String = "Invalid";
              break;
          } //switch statement for die1 = 4 with all the different options for die2
        case 5:
           switch(die2){
             case 1: die2String = "Easy Six";
               break;
             case 2: die2String = "Seven Out";
               break;
             case 3: die2String = "Easy Eight";
               break;
             case 4: die2String = "Nine";
               break;
             case 5: die2String = "Hard Ten";
               break;
             case 6: die2String = "Yo-leven";
               break;
             default: die2String = "Invalid";
               break;
           } //switch statement for die1 = 5 with all the different options for die2
        case 6:
          switch(die2){
            case 1: die2String = "Seven Out";
              break;
            case 2: die2String = "Easy Eight";
              break;
            case 3: die2String = "Nine";
              break;
            case 4: die2String = "Easy Ten";
              break;
            case 5: die2String = "Yo-leven";
              break;
            case 6: die2String = "Boxcars";
              break;
            default: die2String = "Invalid";
              break;
          } //switch statement for die1 = 6 with all the different options for die2
      }
      System.out.println(die2String); 
    }
}