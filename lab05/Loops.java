  /////Paula Gonzalez
  ////Lab05
  //CSE 002
  //October 5, 2018

  import java.util.Scanner;

  ///beginning of code
  public class Loops{
    //main method required for every Java program
    public static void main(String[] args){
      //prints Loops to terminal window       
      Scanner myScanner = new Scanner(System.in);

      int testValue = 1; //initializing testValue

      while(testValue<2){
        System.out.println("What is your course number? ");
        boolean correctInt = myScanner.hasNextInt(); //is it an integer?
          if(correctInt){ //if boolean is true
            int courseNum = myScanner.nextInt(); //requesting an int input
            testValue++ ; //increasing testValue
          }
        else{
          String junkword = myScanner.next();
            System.out.println("Error! Need an input of integer type.");
          }
  } //ends while loop for course number 

      int testValue2 = 1; //initializing testValue2

      while(testValue2<2){
        System.out.println("What is your department name? ");
        boolean correctInt = myScanner.hasNextInt(); //is it an integer?
          if(!correctInt){ //if boolean is true
            String deptName = myScanner.next(); //requesting an int input
            testValue2++ ; //increasing testValue2
          }
        else{
          String junkword = myScanner.next(); //erases what came previously in the code
            System.out.println("Error! Need an input of string type.");
          }
  } //ends while loop for department name 

    int testValue3 = 1; //initializing testValue3

   while(testValue3<2){
        System.out.println("How many times do you meet per week? ");
        boolean correctInt = myScanner.hasNextInt(); //is it an integer?
          if(correctInt){ //if boolean is true
            int meets = myScanner.nextInt(); //requesting an int input
            testValue3++ ; //increasing testValue3
          }
        else{
          String junkword = myScanner.next();
            System.out.println("Error! Need an input of integer type.");
    }
  } //ends while loop for times meet per week

      int testValue4 = 1; //initializing testValue4

      while(testValue4<2){
        System.out.println("What time does your class start? ");
        boolean correctInt = myScanner.hasNextInt(); //is it an integer?
          if(correctInt){ //if boolean is true
            int time = myScanner.nextInt(); //requesting an int input
            testValue4++ ; //increasing testValue4
          }
        else{
          String junkword = myScanner.next();
            System.out.println("Error! Need an input of integer type.");
    }
  } //ends while loop for class start time

      int testValue5 = 1; //initializing testValue5

      while(testValue5<2){
        System.out.println("What is your instructor name? ");
        boolean correctInt = myScanner.hasNextInt(); //is it an integer?
          if(!correctInt){ //if boolean is true
            String instructorName = myScanner.next(); //requesting an int input
            testValue5++ ; //increasing testValue5
          }
        else{
          String junkword = myScanner.next();
            System.out.println("Error! Need an input of string type.");
    }
  } //ends while loop for instructor name

      int testValue6 = 1; //initializing testValue6

    while(testValue6<2){
        System.out.println("How many students? ");
        boolean correctInt = myScanner.hasNextInt(); //is it an integer?
          if(correctInt){ //if boolean is true
            int numOfStudents = myScanner.nextInt(); //requesting an int input
            testValue6++ ; //increasing testValue6
          }
        else{
          String junkword = myScanner.next();
            System.out.println("Error! Need an input of integer type.");
    }
  } //ends while loop for number of students


    }
  }