 /////Paula Gonzalez
////hw06
//CSE 002 
//October 23, 2018

import java.util.Scanner;

///beginning of code
public class EncryptedX{
  //main method required for every Java program
  public static void main(String[] args){
    //prints EncryptedX to terminal window  
    Scanner myScanner = new Scanner(System.in); //create a scanner      
    int rows = 1; //assigning a value to rows     

    System.out.print("Enter an integer between 1 and 100: "); //prompt     

    while (!(myScanner.hasNextInt())) { //while the entered item is not an integer       
      System.out.println("Error! Need an input of integer type."); //prints error if not an integer     
      String junkword = myScanner.next(); //lets you input a value //accepts a value          
      System.out.print("Enter an integer between 1 and 100: ");  //prompts again  
  }  

    rows = myScanner.nextInt(); //scan the next input 

    while(rows<0 || rows>100) { //while the number is between 1 and 10    
      System.out.println("Error! Need an input of integer type between 1 and 100."); //prints error if integer not between 1 and 10      
      System.out.print("Enter an integer between 1 and 100");  //prompts again 
      rows = myScanner.nextInt(); //takes has.nextint and inputs the value into your code     
  }

    for(int i = 0; i < rows; i++){ //for loop for rows
      for(int j = 0; j < rows; j++){ //for loop for columns
        if(i == j){ //if statement for top left to bottom right spaces 
          System.out.print(" "); //print spaces
        }
        else if(rows - (i + j) == 1){ //if statement for bottom left to top right spaces
          System.out.print(" "); //print spaces
        }
        else{ //else statement
          System.out.print("*"); //prints out *
        }
  }
      System.out.println(); //go onto new line
}
  }
}