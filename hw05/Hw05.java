  /////Paula Gonzalez
  ////Hw05
  //CSE 002 
  //October 9, 2018

  import java.util.Scanner;
  import java.util.Random;

  ///beginning of code
  public class Hw05{
    //main method required for every Java program
    public static void main(String[] args){
      //prints hw05 to terminal window       
      Scanner myScanner = new Scanner(System.in);
      System.out.println("How many times should you generate hands? ");
      int hands = myScanner.nextInt();
      int i = 0; //initializing value of i
      double numOfOccurences4ofKind = 0; //initializing number of occurences of 4 of a kind
      double numOfOccurences3ofKind = 0; //initializing number of occurences of 3 of a kind
      double numOfOccurences2Pair = 0; //initializing number of occurences of 2 Pair
      double numOfOccurences1Pair = 0; //initializing number of occurences of 1 Pair
      int counter = 0;


      while(i < hands){
      int upperBound = 52; //assigned the upper bound
      int baseNum = 1; //assigned baseNum
      int card1 = (int)(Math.random()*(upperBound))+baseNum; //assigned card

      String cardString1 = ""; 
      switch (card1%13){
        case 1: cardString1 = "Ace";
          break;
        case 2: cardString1 = "2";
          break;
        case 3: cardString1 = "3";
          break;
        case 4: cardString1 = "4";
          break;  
        case 5: cardString1 = "5";
          break;
        case 6: cardString1 = "6";
          break;
        case 7: cardString1 = "7";
          break;
        case 8: cardString1 = "8";
          break;
        case 9: cardString1 = "9";
          break;
        case 10: cardString1 = "10";
          break;
        case 11: cardString1 = "Jack";
          break;
        case 12: cardString1 = "Queen";
          break;
        case 0: cardString1 = "King";
          break;

      } //ends switch statement for the first card that is drawn

      int card2 = (int)(Math.random()*(upperBound))+baseNum; //assigned card

      String cardString2 = "";
      switch (card2%13){
        case 1: cardString2 = "Ace";
          break;
        case 2: cardString2 = "2";
          break;
        case 3: cardString2 = "3";
          break;
        case 4: cardString2 = "4";
          break;  
        case 5: cardString2 = "5";
          break;
        case 6: cardString2 = "6";
          break;
        case 7: cardString2 = "7";
          break;
        case 8: cardString2 = "8";
          break;
        case 9: cardString2 = "9";
          break;
        case 10: cardString2 = "10";
          break;
        case 11: cardString2 = "Jack";
          break;
        case 12: cardString2 = "Queen";
          break;
        case 0: cardString2 = "King";
          break;

      } //ends switch statement for the second card that is drawn

      int card3 = (int)(Math.random()*(upperBound))+baseNum; //assigned card

      String cardString3 = "";
      switch (card3%13){
        case 1: cardString3 = "Ace";
          break;
        case 2: cardString3 = "2";
          break;
        case 3: cardString3 = "3";
          break;
        case 4: cardString3 = "4";
          break;  
        case 5: cardString3 = "5";
          break;
        case 6: cardString3 = "6";
          break;
        case 7: cardString3 = "7";
          break;
        case 8: cardString3 = "8";
          break;
        case 9: cardString3 = "9";
          break;
        case 10: cardString3 = "10";
          break;
        case 11: cardString3 = "Jack";
          break;
        case 12: cardString3 = "Queen";
          break;
        case 0: cardString3 = "King";
          break;

      } //ends switch statement for the third card that is drawn

      int card4 = (int)(Math.random()*(upperBound))+baseNum; //assigned card

      String cardString4 = "";
      switch (card4%13){
        case 1: cardString4 = "Ace";
          break;
        case 2: cardString4 = "2";
          break;
        case 3: cardString4 = "3";
          break;
        case 4: cardString4 = "4";
          break;  
        case 5: cardString4 = "5";
          break;
        case 6: cardString4 = "6";
          break;
        case 7: cardString4 = "7";
          break;
        case 8: cardString4 = "8";
          break;
        case 9: cardString4 = "9";
          break;
        case 10: cardString4 = "10";
          break;
        case 11: cardString4 = "Jack";
          break;
        case 12: cardString4 = "Queen";
          break;
        case 0: cardString4 = "King";
          break;

      } //ends switch statement for the fourth card that is drawn

      int card5 = (int)(Math.random()*(upperBound))+baseNum; //assigned card

      String cardString5 = "";
      switch (card5%13){
        case 1: cardString5 = "Ace";
          break;
        case 2: cardString5 = "2";
          break;
        case 3: cardString5 = "3";
          break;
        case 4: cardString5 = "4";
          break;  
        case 5: cardString5 = "5";
          break;
        case 6: cardString5 = "6";
          break;
        case 7: cardString5 = "7";
          break;
        case 8: cardString5 = "8";
          break;
        case 9: cardString5 = "9";
          break;
        case 10: cardString5 = "10";
          break;
        case 11: cardString5 = "Jack";
          break;
        case 12: cardString5 = "Queen";
          break;
        case 0: cardString5 = "King";
          break;

      } //ends switch statement for the fifth card that is drawn

      System.out.println(cardString1 + " " + cardString2 + " " + cardString3 + " " + cardString4 + " " + cardString5); //prints out what cards were randomly selected


          if(cardString1==cardString2 && cardString3==cardString4 || cardString3==cardString4 && cardString1==cardString2){
            counter++; //increasing counter
            ++numOfOccurences2Pair; //accounts for how many times there's 2 Pair
          }
           else if(cardString1==cardString2 && cardString4==cardString5 || cardString4==cardString5 && cardString1==cardString2){
            counter++; //increasing counter           
            ++numOfOccurences2Pair; //accounts for how many times there's 2 Pair
          }
           else if(cardString1==cardString2 && cardString3==cardString5 || cardString3==cardString5 && cardString1==cardString2){
            counter++; //increasing counter          
            ++numOfOccurences2Pair; //accounts for how many times there's 2 Pair
          }
           else if(cardString4==cardString5 && cardString2==cardString3 || cardString2==cardString3 && cardString4==cardString5){
            counter++; //increasing counter        
            ++numOfOccurences2Pair; //accounts for how many times there's 2 Pair
          }
             else if(cardString4==cardString5 && cardString1==cardString3 || cardString1==cardString3 && cardString4==cardString5){
            counter++; //increasing counter          
            ++numOfOccurences2Pair; //accounts for how many times there's 2 Pair
          }
           else if(cardString1==cardString3 && cardString2==cardString4 || cardString2==cardString4 && cardString1==cardString3){
            counter++; //increasing counter          
            ++numOfOccurences2Pair; //accounts for how many times there's 2 Pair
          }
           else if(cardString1==cardString3 && cardString2==cardString5 || cardString2==cardString5 && cardString1==cardString3){
            counter++; //increasing counter          
            ++numOfOccurences2Pair; //accounts for how many times there's 2 Pair
          }
            else if(cardString1==cardString2 && cardString2==cardString3 && cardString3==cardString4){ 
              counter++; //increasing counter
              ++numOfOccurences4ofKind; //accounts for how many times there's 4 of a Kind
            }   
            else if(cardString1==cardString2 && cardString2==cardString4 && cardString4==cardString5){ 
              counter++; //increasing counter
              ++numOfOccurences4ofKind; //accounts for how many times there's 4 of a Kind
            }   
            else if(cardString2==cardString3 && cardString3==cardString4 && cardString4==cardString5){ 
              counter++; //increasing counter
              ++numOfOccurences4ofKind; //accounts for how many times there's 4 of a Kind
            }   
            else if(cardString1==cardString3 && cardString3==cardString4 && cardString4==cardString5){ 
              counter++; //increasing counter
              ++numOfOccurences4ofKind; //accounts for how many times there's 4 of a Kind
            }
            else if(cardString1==cardString2 && cardString2==cardString3){
              counter++; //increasing counter
              ++numOfOccurences3ofKind; //accounts for how many times there's 3 of a Kind
            } 
            else if(cardString1==cardString3 && cardString3==cardString4){
              counter++; //increasing counter
              ++numOfOccurences3ofKind; //accounts for how many times there's 3 of a Kind
            } 
            else if(cardString1==cardString4 && cardString4==cardString5){
              counter++; //increasing counter
              ++numOfOccurences3ofKind; //accounts for how many times there's 3 of a Kind
            }
            else if(cardString2==cardString3 && cardString3==cardString4){
              counter++; //increasing counter
              ++numOfOccurences3ofKind; //accounts for how many times there's 3 of a Kind
            }
            else if(cardString3==cardString4 && cardString4==cardString5){
              counter++; //increasing counter
              ++numOfOccurences3ofKind; //accounts for how many times there's 3 of a Kind
            }
            else if(cardString1==cardString4 && cardString2==cardString3){
              counter++; //increasing counter
              ++numOfOccurences3ofKind; //accounts for how many times there's 3 of a Kind
            }
            else if(cardString1==cardString5 && cardString2==cardString3){
              counter++; //increasing counter
              ++numOfOccurences3ofKind; //accounts for how many times there's 3 of a Kind
            }
            else if(cardString1==cardString5 && cardString2==cardString4){
              counter++; //increasing counter
              ++numOfOccurences3ofKind; //accounts for how many times there's 3 of a Kind
            }
            else if(cardString1==cardString2){
              counter++; //increasing counter
              ++numOfOccurences1Pair; //accounts for how many times there's 1 Pair
            }
            else if(cardString1==cardString3){
              counter++; //increasing counter
              ++numOfOccurences1Pair; //accounts for how many times there's 1 Pair
            }
            else if(cardString1==cardString4){
              counter++; //increasing counter
              ++numOfOccurences1Pair; //accounts for how many times there's 1 Pair
            }
            else if(cardString1==cardString5){
              counter++; //increasing counter
              ++numOfOccurences1Pair; //accounts for how many times there's 1 Pair
            }
            else if(cardString2==cardString3){
              counter++; //increasing counter
              ++numOfOccurences1Pair; //accounts for how many times there's 1 Pair
            }
            else if(cardString2==cardString4){
              counter++; //increasing counter
              ++numOfOccurences1Pair; //accounts for how many times there's 1 Pair
            }
            else if(cardString2==cardString5){
              counter++; //increasing counter
              ++numOfOccurences1Pair; //accounts for how many times there's 1 Pair
            }
            else if(cardString3==cardString4){
              counter++; //increasing counter
              ++numOfOccurences1Pair; //accounts for how many times there's 1 Pair
            }
            else if(cardString3==cardString5){
              counter++; //increasing counter
              ++numOfOccurences1Pair; //accounts for how many times there's 1 Pair
            }
            else if(cardString4==cardString5){
              counter++; //increasing counter
              ++numOfOccurences1Pair; //accounts for how many times there's 1 Pair
            }
            else{
              System.out.println("No matches"); //doesn't match any of the previously stated above
            }
       i++; //increase i in order to go through loop again   
       }

      System.out.println("The number of loops: "+ hands);
      double probability1 = numOfOccurences4ofKind / hands; //initializing probability 1
      System.out.print("The probability of Four-of-a-kind: ");
      System.out.printf("%.3f\n", probability1); //printing 3 decimal probability of 4 of a Kind
      double probability2 = numOfOccurences3ofKind / hands; //initializing probability 2 
      System.out.print("The probability of Three-of-a-kind: ");
      System.out.printf("%.3f\n", probability2); //printing 3 decimal probability of 3 of a Kind
      double probability3 = numOfOccurences2Pair / hands; //initializing probability 3
      System.out.printf("The probability of Two-Pair: ");     
      System.out.printf("%.3f\n", probability3); //printing 3 decimal probability of 2 Pair
      double probability4 = numOfOccurences1Pair/ hands; //initializing probability 4
      System.out.printf("The probability of One-Pair: " );     
      System.out.printf("%.3f\n", probability4); //printing 3 decimal probability of 1 Pair
      } //ends main method

  } //ends class method