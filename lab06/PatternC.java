   /////Paula Gonzalez
  ////lab06
  //CSE 002 
  //October 12, 2018

import java.util.Scanner;

  ///beginning of code
  public class PatternC{
    //main method required for every Java program
    public static void main(String[] args){
      //prints PatternC to terminal window  
      Scanner myScanner = new Scanner(System.in); //create a scanner      
      int rows = 1; //assigning a value to rows     
      
      String spaces = new String("          "); //assigning values to string spaces         
      System.out.print("Enter an integer between 1 and 10: "); //prompt     
      
      while (!(myScanner.hasNextInt())) { //while the entered item is not an integer       
      System.out.println("Error! Need an input of integer type."); //prints error if not an integer     
      String junkword = myScanner.next(); //lets you input a value          
      System.out.print("Enter an integer between 1 and 10: ");  //prompts again  
    }  
      
      rows = myScanner.nextInt(); //scan the next input 
      
      while(rows<1 || rows>10) { //while the number is between 1 and 10    
      System.out.println("Error! Need an input of integer type between 1 and 10."); //prints error if integer not between 1 and 10       
      System.out.print("Enter an integer between 1 and 10: ");  //prompts again 
      rows = myScanner.nextInt(); //scan next input     
    }

            for (int j=1; j<=rows; j++) { //for loop for j      
               System.out.print(spaces); //prints out number of spaces needed     
                 for (int i=j; i>0; i--){ //for loop for i       
                   System.out.print(i); //prints i
      }      
        System.out.println(); //go to a new line      
        spaces = spaces.substring(0, spaces.length() - 1); //removes a space each time  //used google to find this formula 
      }   
    }
}