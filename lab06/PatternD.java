   /////Paula Gonzalez
  ////lab06
  //CSE 002 
  //October 12, 2018

 
import java.util.Scanner;

  ///beginning of code
  public class PatternD{
    //main method required for every Java program
    public static void main(String[] args){
      //prints PatternD to terminal window  
      Scanner myScanner = new Scanner(System.in); //create a scanner      
      int rows = 1; //assigning a value to rows     
      
      String spaces = new String("          "); //assigning values to string spaces         
      System.out.print("Enter an integer between 1 and 10: "); //prompt     
      
      while (!(myScanner.hasNextInt())) { //while the entered item is not an integer       
      System.out.println("Error! Need an input of integer type."); //prints error if not an integer     
      String junkword = myScanner.next(); //lets you input a value          
      System.out.print("Enter an integer between 1 and 10: ");  //prompts again  
    }  
      
    rows = myScanner.nextInt(); //scan the next input 
    
    while(rows<1 || rows>10) { //while the number is between 1 and 10    
      System.out.println("Error! Need an input of integer type between 1 and 10."); //prints error if integer not between 1 and 10       
      System.out.print("Enter an integer between 1 and 10: ");  //prompts again
      rows = myScanner.nextInt(); //scan next input 
    }   
        
          for (int i = 1; i <= rows; ++i){ //for loop for variable i
          for (int j = rows - i + 1; j >= 1; --j){ //for loop for variable j
            System.out.print (j + " "); //print statement for what should take place inside of this for loop
          }
          System.out.println(); //go onto the next line
        }
      }
    }
  

